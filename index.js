var express = require("express");
var alexa = require("alexa-app");
var Promise = require("bluebird");
Promise.promisifyAll(require("request"))
var request = require("request");
var PORT = process.env.PORT || 8080;
var app = express();
var data = [];
var AmazonSpeech = require('ssml-builder/amazon_speech');
// ALWAYS setup the alexa app and attach it to express before anything else.
var alexaApp = new alexa.app("cumulocity");



var status_reporter = require('./status_reporter');
var devices_mod = require('./devices');

var sys_url = "https://yashwanth.cumulocity.com";
var base64_encode = "";

function process_news(body){
           var leng  = body.results.length;
          
          for(let i=0;i<5;i++){
           data.push(body.results[i].headline);

         }
        return data;
};


function get_data_or_handle_error(c){

     data = [];
    var p = request.getAsync(sys_url);
    p.then(function (resp) {
           data = process_news(JSON.parse(resp.body));
           //console.log(data);
          c(data);
    });
    return p;

}

function build_news(news){
          var speech = new AmazonSpeech();
          speech.say("Status of Devices,");
          speech.pause('1s');
       news.forEach(function(n){
        speech.say(n);
        speech.pause("1s");

       });  
       return speech;

}





alexaApp.express({
  expressApp: app,
  checkCert: false,
  debug: true
});

app.set("view engine", "ejs");

alexaApp.launch(function(request, response) {
  
     /*return get_data_or_handle_error(function(data){
     var speech = build_news(data);         
     var speechOutput = speech.ssml();
     response.say(speechOutput);
     }); */
	var helpOutput = "You can say 'get status'. You can also say 'stop' or 'exit' to 'quit'.";
    
    // AMAZON.HelpIntent must leave session open -> .shouldEndSession(false)
    response.say(helpOutput).shouldEndSession(false);

});


alexaApp.intent("cy", {
    "slots": { },
    "utterances": [
      "get status",
	  "to get status"
    ]
  },
  function(request, response) {
	  
	 return status_reporter.getStatus(response);	 
  
  }
);


alexaApp.intent("devices", {
    "slots": [
	{
          "name": "DeviceNum",
          "type": "AMAZON.NUMBER"
        }
	],
    "utterances": [
      "to register a device {-|DeviceNum}",
	  "to add a device {-|DeviceNum}",
	  "to create a device {-|DeviceNum}"
    ]
  },
  function(request, response) {
	 var deviceNum = request.slot("DeviceNum");
	 console.log("deviceid",deviceNum);
	 if(deviceNum){ 
          return devices_mod.createDevices(deviceNum,response);
       } else {
	       response.say("sorry, but you need to provide me a valid device id, please try again").shouldEndSession(true);
    }
  }
 );

alexaApp.intent("devicesRemove",{
	"slots":[{
          "name": "DeviceNum",
          "type": "AMAZON.NUMBER"
        }],
	"utterances":[
          "remove device {DeviceNum}"
	]
},function(request, response){
	
	
	
}); 
 
 



alexaApp.intent("alarms", {
    "slots": [
	   {
          "name": "DeviceNum",
          "type": "AMAZON.NUMBER"
        }
	],
    "utterances": [
      "to get alarms",
      "get alarms for device {DeviceNum}",
	  "to get critical alarms"
    ]
  },
  function(request, response) {
	  
     
	 var mesg;
	 var device_num = request.slot("DeviceNum");
	 console.log("no device name",device_num);
	 if(device_num){
		 alarm_mod.getAlarmsForDevice(device_num,response); 
		 
	 } else {
	     alarm_mod.getAlarms(response);
	 } 
     
	 
      
  }
);


alexaApp.error = function(exception, request, response) {
	console.log(exception);
	console.log(request.data);
  response.say("Sorry, something bad happened");
};


alexaApp.intent("AMAZON.HelpIntent", {
    "slots": {},
    "utterances": []
  },
  function(request, response) {
    var helpOutput = "You can say 'for devices status'. You can also say stop or exit to quit.";
    
    // AMAZON.HelpIntent must leave session open -> .shouldEndSession(false)
    response.say(helpOutput).shouldEndSession(false);
  }
);
 
alexaApp.intent("AMAZON.StopIntent", {
    "slots": {},
    "utterances": []
  }, function(request, response) {
    var stopOutput = "No Problem. Thank you";
    response.say(stopOutput);
  }
);
 
alexaApp.intent("AMAZON.CancelIntent", {
    "slots": {},
    "utterances": []
  }, function(request, response) {
    var cancelOutput = "No problem. Request canceled.";
    response.say(cancelOutput);
  }
);




//console.log(alarm_mod);

app.listen(PORT, () => console.log("Listening on port " + PORT + "."));

     /*get_data_or_handle_error(function(data){
     var speech = build_news(data);         
     var speechOutput = speech.ssml();
      console.log(speechOutput);//response.say(speechOutput);
     });*/ 
